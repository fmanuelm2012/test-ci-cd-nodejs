"use strict";

var express = require("express");
var app = express();
var port = 4000;
app.get("/", function (req, res) {
  res.send("Hello everybody");
});
app.listen(port, function () {
  console.log("The server is running on port ".concat(port));
});