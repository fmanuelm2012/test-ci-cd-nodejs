const express = require("express");

const app = express();

const port = 4000;

app.get("/", (req, res)=>{
	res.send("Hello everybody");
});

app.listen(port, ()=>{
	console.log(`The server is running on port ${port}`);
});